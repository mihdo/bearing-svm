ewa <- function(x, alpha) {
  # Exponentially weighted average
  # Inputs:
  #   x: 1-d vector
  #   alpha: decay factor
  # Output:
  #   Exponentially weighted average computed over x
  
  mean(x*alpha^((length(x)-1):0))/mean(alpha^((length(x)-1):0))
}
