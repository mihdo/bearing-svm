# SVM classification of recording features
# One-class SVM (semisupervised) and supervised SVM with three classes
library(e1071)
library(caret)
library(zoo)
source("ewa.R")

# Directory containing the learning set
learning.dir <- "../features/Learning_set/"
learning.dataset <- list.files(learning.dir)

# Combined learning set data frame
learning.df <- data.frame(matrix(nrow = 0, ncol = 0))
for (dataset in learning.dataset) {
  temp.dir <- paste(learning.dir, dataset, sep = "")
  temp.df <- read.csv(temp.dir)
  learning.df <- rbind(learning.df, temp.df)
}

# Shuffle learning set data frame before training
learning.df <- learning.df[sample(nrow(learning.df)),]

# Split learning set into training and test sets
training.split <- 0.7 # percentage of learning set to use for training
training.df <- learning.df[1:floor(training.split*nrow(learning.df)),]
test.df <- learning.df[ceiling(training.split*nrow(learning.df)):nrow(learning.df),]

#### One-class SVM ####
# Because of the nature of one-class classification, testing is done on the 
# two opposite ends of the data distribution to maximize accuracy 
# of error estimation.
training.df.oneclass <- training.df[which(training.df$RUL > quantile(training.df$RUL, 0.5)),
                                    !(names(training.df) %in% "RUL")]
test.df.oneclass.positive <- test.df[which(test.df$RUL > quantile(training.df$RUL, 0.7)),
                                     !(names(test.df) %in% "RUL")]
test.df.oneclass.negative <- test.df[which(test.df$RUL < quantile(training.df$RUL, 0.3)),
                                     !(names(test.df) %in% "RUL")]

# Train one-class svm
svm.oneclass <- svm(training.df.oneclass, y = NULL,
                    type = "one-classification",
                    kernel = "radial")

# Estimate true positive and true negative accuracies
oneclass.true.positive <- 
  sum(predict(svm.oneclass, test.df.oneclass.positive))/nrow(test.df.oneclass.positive)
oneclass.true.negative <-
  1 - sum(predict(svm.oneclass, test.df.oneclass.negative))/nrow(test.df.oneclass.negative)

# Estimate and plot bearing health scores on test set
# Apply exponential weighting to account for previous estimates as well as
# max pooling to reduce estimation noise
test.dir <- "../features/Full_Test_Set/"
test.dataset <- list.files(test.dir)
for (dataset in test.dataset) {
  temp.dir <- paste(test.dir, dataset, sep = "")
  temp.df <- read.csv(temp.dir)
  svm.predict <- predict(svm.oneclass, temp.df)
  svm.predict <- rollapply(svm.predict, 5, max, align = "center", partial = T)
  svm.predict <- rollapply(svm.predict, 35, ewa, 0.95)
  plot.dir <- paste("../plots/model/ocsvm_", substr(dataset, 1, 10), ".png", sep = "")
  png(plot.dir, height = 360)
  plot(1:length(svm.predict), svm.predict, 
       xlab = "Recording", ylab = "Bearing health", 
       main = substr(dataset, 1, 10), type = "l")
  dev.off()
}

#### SVM with 3 constructed classes ####
# Create classes and bind them to training and test data frames
# Training set
class <- rep(1, nrow(training.df))
class[which(training.df$RUL < quantile(training.df$RUL, 0.4))] <- 2
class[which(training.df$RUL < quantile(training.df$RUL, 0.1))] <- 3
class <- factor(class)
training.df.c <- cbind(class, training.df[,!(names(training.df) %in% "RUL")])

# Test set
class <- rep(1, nrow(test.df))
class[which(test.df$RUL < quantile(training.df$RUL, 0.4))] <- 2
class[which(test.df$RUL < quantile(training.df$RUL, 0.1))] <- 3
class <- factor(class)
test.df.c <- cbind(class, test.df[,!(names(test.df) %in% "RUL")])

# Train model
svm.model <- svm(class ~ .,
                 data = training.df.c,
                 kernel = "radial")

# Compute accuracy and confusion matrix
confusion.matrix <- confusionMatrix(predict(svm.model, test.df.c), test.df.c$class)

# Estimate and plot bearing condition scores on test set
testing.dir <- "../features/Full_Test_Set/"
testing.dataset <- list.files(testing.dir)
for (dataset in testing.dataset) {
  temp.dir <- paste(testing.dir, dataset, sep = "")
  temp.df <- read.csv(temp.dir)
  svm.predict <- predict(svm.model, temp.df)
  svm.predict <- rollapply(as.numeric(svm.predict), 35, ewa, 0.95, align = "left", partial = T)
  plot.dir <- paste("../plots/model/svm_", substr(dataset, 1, 10), ".png", sep = "")
  png(plot.dir, height = 360)
  plot(1:length(svm.predict), svm.predict,
       xlab = "Recording", ylab = "Bearing condition",
       main = substr(dataset, 1, 10), type = "l")
  dev.off()
}
